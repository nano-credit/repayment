package sn.free.mfs.domain;


import lombok.*;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A RepaymentFile.
 */
//@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "repayment_file")
public class RepaymentFile extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "loan_file_id", nullable = false, unique = true)
    private Long loanFileID;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @Column(name = "identification_no")
    private String identificationNo;

    @NotNull
    @Column(name = "offer_slug", nullable = false)
    private String offerSlug;

    @NotNull
    @Column(name = "initial_capital", nullable = false)
    private Double initialCapital;

    @NotNull
    @Column(name = "initial_due_date", nullable = false)
    private Instant initialDueDate;

    @NotNull
    @Column(name = "application_fees", nullable = false)
    private Double applicationFees;

    @Column(name = "pr_1_fees")
    private Double pr1Fees;

    @Column(name = "pr_2_fees")
    private Double pr2Fees;

    @Column(name = "capital_outstanding")
    private Double capitalOutstanding;

    @Column(name = "fees_outstanding")
    private Double feesOutstanding;

    @Column(name = "pr_1_outstanding")
    private Double pr1Outstanding;

    @Column(name = "pr_2_outstanding")
    private Double pr2Outstanding;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "repayment_state", nullable = false)
    private RepaymentState repaymentState;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "repayment_status", nullable = false)
    private RepaymentStatus repaymentStatus;

    @NotNull
    @Column(name = "due_date", nullable = false)
    private Instant dueDate;

    @OneToMany(mappedBy = "repaymentFile")
    private Set<RepaymentTransfer> transfers = new HashSet<>();

    @NotNull
    @Column(name = "default_loan_duration", nullable = false)
    private Integer defaultLoanDuration;

    @NotNull
    @Column(name = "nb_jour_pr1", nullable = false)
    private Integer nbJourPr1;

    @NotNull
    @Column(name = "nb_jour_pr2", nullable = false)
    private Integer nbJourPr2;

    @NotNull
    @Column(name = "pr_0_rate", nullable = false)
    private Integer pr0Rate;

    @NotNull
    @Column(name = "pr_1_rate", nullable = false)
    private Integer pr1Rate;

    @NotNull
    @Column(name = "pr_2_rate", nullable = false)
    private Integer pr2Rate;

    @Column(name = "rappel_pr0")
    private String rappelPr0;

    @Column(name = "rappel_pr1")
    private String rappelPr1;

    @Column(name = "rappel_pr2")
    private String rappelPr2;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RepaymentFile)) {
            return false;
        }
        return id != null && id.equals(((RepaymentFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
