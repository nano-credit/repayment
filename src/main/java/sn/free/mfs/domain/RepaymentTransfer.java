package sn.free.mfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.TransferType;

import sn.free.mfs.domain.enumeration.PaymentState;

/**
 * A RepaymentTransfer.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "repayment_transfer")
public class RepaymentTransfer extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "source_wallet", nullable = false)
    private String sourceWallet;

    @Column(name = "source_pin")
    private String sourcePin;

    @NotNull
    @Column(name = "target_wallet", nullable = false)
    private String targetWallet;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TransferType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_state")
    private PaymentState paymentState;

    @Column(name = "payment_tx_id")
    private String paymentTxId;

    @Column(name = "payment_message")
    private String paymentMessage;

    @ManyToOne
    @JsonIgnoreProperties("transfers")
    private RepaymentFile repaymentFile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
