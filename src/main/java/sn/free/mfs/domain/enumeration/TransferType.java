package sn.free.mfs.domain.enumeration;

/**
 * The TransferType enumeration.
 */
public enum TransferType {
    PARTIAL, COMPLETE, FDD, CAPITAL, PR1, PR2
}
