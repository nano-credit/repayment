package sn.free.mfs.domain.enumeration;

/**
 * The RepaymentState enumeration.
 */
public enum RepaymentState {
    ENCOURS, PR1, PR2, WRITE_OFF, CLOSED
}
