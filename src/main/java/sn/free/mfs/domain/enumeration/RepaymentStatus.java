package sn.free.mfs.domain.enumeration;

/**
 * The RepaymentStatus enumeration.
 */
public enum RepaymentStatus {
    NA, PARTIALLY, IN_ADVANCE, TERM, UTIMATELY, PR1, PR2, RECOVERED, OVERDUE
}
