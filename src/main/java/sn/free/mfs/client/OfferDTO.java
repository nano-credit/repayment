package sn.free.mfs.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @project: microcredit
 * @author: psow on 17/06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfferDTO implements Serializable {

    private Long id;

    @NotNull
    private String tenantId;

    @NotNull
    private String offerName;

    private Boolean activated;

    private Integer freeSeniority;

    private Integer freemoneySeniority;

    private Integer defaultLoanDuration;

    private Integer minCustAge;

    private Integer maxCustAge;

    private Integer maxSimultaneousLoan;

    private Integer maxLoanPerDate;

    private Integer maxCurrentBalance;

    private Integer minAmount;

    private Integer maxAmount;

    @NotNull
    private Integer totalFeesRate;

    @NotNull
    private Integer minRepayment;

    @NotNull
    private Integer pr0Rate;

    @NotNull
    private Integer pr1Rate;

    @NotNull
    private Integer pr2Rate;

    private String msisdnAdmin;

    @NotNull
    private String emailAdmin;


    private Long loanWalletId;

    private Long repaymentWalletId;

    private Long feesWalletId;

    private WalletDTO loanWallet;

    private WalletDTO repaymentWallet;

    private WalletDTO feesWallet;

    @NotNull
    private Integer feesFreeMoneyRate;

    @NotNull
    private Integer feesBankRate;

    @NotNull
    private Integer nbJourPr1;

    @NotNull
    private Integer nbJourPr2;

    private String rappelPr0;

    private String rappelPr1;

    private String rappelPr2;

}
