package sn.free.mfs.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.AccessStatus;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link AccessListDTO} entity.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccessListDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String cin;

    private String firstName;

    private String lastName;

    private String email;

    private AccessStatus status;
}
