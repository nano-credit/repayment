package sn.free.mfs.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @project: microcredit
 * @author: psow on 17/06/2020
 */
@FeignClient("https://microcredit")
public interface GatewayConfigFeignClient {

    @RequestMapping(value = "/api/offers/slug/{slug}", method = RequestMethod.GET)
    ResponseEntity<OfferDTO> getOfferConfig(@PathVariable("slug") String slug);

    @RequestMapping(value = "/api/offers", method = RequestMethod.GET)
    ResponseEntity<List<OfferDTO>> getOfferConfig();

    @RequestMapping(value = "/api/access-lists/blacklist/{msisdn}/{cni}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<AccessListDTO> blackListUnpayer(@PathVariable("msisdn") String msisdn, @PathVariable("cni") String cni, @RequestHeader("Authorization") String jwt);
}
