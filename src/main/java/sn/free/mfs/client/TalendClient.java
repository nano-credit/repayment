package sn.free.mfs.client;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

@Component
public class TalendClient {

    @Value("${app.talend.host}")
    private String talendHost;
    @Value("${app.talend.sendSmsEndpoint}")
    private String sendSmsEndpoint;

    public boolean sendSms(String receiverMsisdn, String messageTosend){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject sendSmsRequest = new JSONObject();
        try {
            sendSmsRequest.put("messageToSend", messageTosend);
            sendSmsRequest.put("receiverMSISDN", receiverMsisdn);
            sendSmsRequest.put("sender", "FREE MONEY");
            sendSmsRequest.put("type", 0);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("REQUEST BODY :" + sendSmsRequest.toString());
        HttpEntity<String> request = new HttpEntity<String>(sendSmsRequest.toString(), headers);
        String sendSmsResultStr = "";
        try{
            sendSmsResultStr = restTemplate.postForObject(this.talendHost + this.sendSmsEndpoint, request, String.class);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        System.out.println("RESPONSE BODY :" + sendSmsResultStr);
        return sendSmsResultStr.equalsIgnoreCase("0");
    }

}
