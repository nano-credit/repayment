package sn.free.mfs.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @project: microcredit
 * @author: psow on 22/06/2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WalletDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String pin;
}
