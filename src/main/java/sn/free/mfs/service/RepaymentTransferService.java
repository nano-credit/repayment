package sn.free.mfs.service;

import sn.free.mfs.service.dto.RepaymentTransferDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RepaymentTransfer}.
 */
public interface RepaymentTransferService {

    /**
     * Save a repaymentTransfer.
     *
     * @param repaymentTransferDTO the entity to save.
     * @return the persisted entity.
     */
    RepaymentTransferDTO save(RepaymentTransferDTO repaymentTransferDTO);

    /**
     * Get all the repaymentTransfers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RepaymentTransferDTO> findAll(Pageable pageable);

    /**
     * Get the "id" repaymentTransfer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RepaymentTransferDTO> findOne(Long id);

    /**
     * Delete the "id" repaymentTransfer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find all transfer related to a repayment.
     * @param id of repaymentfile
     * @return list of transfer
     */
    Optional<List<RepaymentTransferDTO>> findRepaymentTransfers(Long id);
}
