package sn.free.mfs.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;

import lombok.*;
import sn.free.mfs.domain.enumeration.TransferType;
import sn.free.mfs.domain.enumeration.PaymentState;

/**
 * A DTO for the {@link sn.free.mfs.domain.RepaymentTransfer} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepaymentTransferDTO implements Serializable {

    private Long id;

    @NotNull
    private String sourceWallet;

    @NotNull
    private String sourcePin;

    @NotNull
    private String targetWallet;

    @NotNull
    private Double amount;

    private TransferType type;

    private PaymentState paymentState;

    private String paymentTxId;

    private String paymentMessage;

    private Long repaymentFileId;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

}
