package sn.free.mfs.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link sn.free.mfs.domain.RepaymentFile} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepaymentFileDTO implements Serializable {

    private Long id;

    @NotNull
    private Long loanFileID;

    @NotNull
    private String offerSlug;

    @NotNull
    private String msisdn;

    private String identificationNo;

    @NotNull
    private Double initialCapital;

    @NotNull
    private Instant initialDueDate;

    @NotNull
    private Double applicationFees;

    private Double pr1Fees;

    private Double pr2Fees;

    private Double capitalOutstanding;

    private Double feesOutstanding;

    private Double pr1Outstanding;

    private Double pr2Outstanding;

    @NotNull
    private RepaymentState repaymentState;

    @NotNull
    private RepaymentStatus repaymentStatus;

    @NotNull
    private Instant dueDate;

    @NotNull
    private Integer defaultLoanDuration;

    @NotNull
    private Integer nbJourPr1;

    @NotNull
    private Integer nbJourPr2;

    private Integer pr0Rate;

    @NotNull
    private Integer pr1Rate;

    @NotNull
    private Integer pr2Rate;

    private String rappelPr0;

    private String rappelPr1;

    private String rappelPr2;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

}
