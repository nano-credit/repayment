package sn.free.mfs.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;
import sn.free.mfs.service.dto.RepaymentFileDTO;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.RepaymentFile}.
 */
public interface RepaymentFileService {

    /**
     * Initiate the repayment file with the given loan file
     *
     * @param repaymentFileDTO with the id of loanFile
     * @return the create repayment file
     */
    RepaymentFileDTO initiate(RepaymentFileDTO repaymentFileDTO);

    /**
     * Save a repaymentFile.
     *
     * @param repaymentFileDTO the entity to save.
     * @return the persisted entity.
     */
    RepaymentFileDTO save(RepaymentFileDTO repaymentFileDTO);

    /**
     * Get all the repaymentFiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RepaymentFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" repaymentFile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RepaymentFileDTO> findOne(Long id);

    /**
     * Delete the "id" repaymentFile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Create required transfer for a payment
     *
     * @param id     of the repayment file
     * @param amount payment amount
     * @return the list of required transfer
     */
    List<RepaymentTransferDTO> payment(Long id, Long amount);

    /**
     * Update repayment file of validated payment
     * @param id
     * @param amount
     * @return
     */
    RepaymentFileDTO paymentOK(Long id, Long amount);


    //Page<RepaymentFileDTO> findByLoanFileIDOrRepaymentStateOrRepaymentStatus(Pageable pageable, Long loanFileId, RepaymentState state, RepaymentStatus status);
    List<RepaymentFileDTO> findByMsisdnOrRepaymentStateOrRepaymentStatusOrDateBetween(Pageable pageable, String msisdn, RepaymentState state, RepaymentStatus status, Instant debut, Instant fin);

}
