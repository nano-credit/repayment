package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RepaymentTransfer} and its DTO {@link RepaymentTransferDTO}.
 */
@Mapper(componentModel = "spring", uses = {RepaymentFileMapper.class})
public interface RepaymentTransferMapper extends EntityMapper<RepaymentTransferDTO, RepaymentTransfer> {

    @Mapping(source = "repaymentFile.id", target = "repaymentFileId")
    RepaymentTransferDTO toDto(RepaymentTransfer repaymentTransfer);

    @Mapping(source = "repaymentFileId", target = "repaymentFile")
    RepaymentTransfer toEntity(RepaymentTransferDTO repaymentTransferDTO);

    default RepaymentTransfer fromId(Long id) {
        if (id == null) {
            return null;
        }
        RepaymentTransfer repaymentTransfer = new RepaymentTransfer();
        repaymentTransfer.setId(id);
        return repaymentTransfer;
    }
}
