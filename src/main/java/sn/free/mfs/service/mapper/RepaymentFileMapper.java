package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.RepaymentFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RepaymentFile} and its DTO {@link RepaymentFileDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RepaymentFileMapper extends EntityMapper<RepaymentFileDTO, RepaymentFile> {


    @Mapping(target = "transfers", ignore = true)
    RepaymentFile toEntity(RepaymentFileDTO repaymentFileDTO);

    default RepaymentFile fromId(Long id) {
        if (id == null) {
            return null;
        }
        RepaymentFile repaymentFile = new RepaymentFile();
        repaymentFile.setId(id);
        return repaymentFile;
    }
}
