package sn.free.mfs.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.mfs.client.*;
import sn.free.mfs.domain.RepaymentFile;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;
import sn.free.mfs.domain.enumeration.TransferType;
import sn.free.mfs.repository.RepaymentFileRepository;
import sn.free.mfs.service.RepaymentFileService;
import sn.free.mfs.service.RepaymentTransferService;
import sn.free.mfs.service.dto.RepaymentFileDTO;
import sn.free.mfs.service.dto.RepaymentTransferDTO;
import sn.free.mfs.service.mapper.RepaymentFileMapper;
import sn.free.mfs.service.utils.RepaymentChain;
import sn.free.mfs.service.utils.TransferConfigWrapper;

import javax.validation.constraints.NotNull;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service Implementation for managing {@link RepaymentFile}.
 */
@Service
@Transactional
@Slf4j
public class RepaymentFileServiceImpl implements RepaymentFileService {


    private final RepaymentFileRepository repaymentFileRepository;
    private final RepaymentFileMapper repaymentFileMapper;
    private final GatewayConfigFeignClient gatewayClient;
    private final RepaymentTransferService repaymentTransferService;


    @Value("${app.wallets.central-repayment.msisdn:}")
    private String centralMsisdn;
    @Value("${app.wallets.central-repayment.pin:}")
    private String centralPin;
    @Value("${app.wallets.freemoney-fees.msisdn:}")
    private String fmFeesMsisdn;
    @Value("${app.wallets.freemoney-fees.pin:}")
    private String fmFeesPin;
    @Value("${app.jwt}")
    private String jwt;

    private final TalendClient talendClient;

    public RepaymentFileServiceImpl(RepaymentFileRepository repaymentFileRepository, RepaymentFileMapper repaymentFileMapper, GatewayConfigFeignClient gatewayClient, RepaymentTransferService repaymentTransferService, TalendClient talendClient) {
        this.repaymentFileRepository = repaymentFileRepository;
        this.repaymentFileMapper = repaymentFileMapper;
        this.gatewayClient = gatewayClient;
        this.repaymentTransferService = repaymentTransferService;
        this.talendClient = talendClient;
    }

    @Override
    public RepaymentFileDTO initiate(RepaymentFileDTO repaymentFileDTO) {
        return null;
    }

    /**
     * Save a repaymentFile.
     *
     * @param repaymentFileDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RepaymentFileDTO save(RepaymentFileDTO repaymentFileDTO) {
        log.debug("Request to save RepaymentFile : {}", repaymentFileDTO);
        RepaymentFile repaymentFile = repaymentFileMapper.toEntity(repaymentFileDTO);
        repaymentFile = repaymentFileRepository.save(repaymentFile);
        return repaymentFileMapper.toDto(repaymentFile);
    }

    /**
     * Get all the repaymentFiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RepaymentFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RepaymentFiles");
        return repaymentFileRepository.findAllByOrderByCreatedByDesc(pageable)
            .map(repaymentFileMapper::toDto);
    }

    /**
     * Get one repaymentFile by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RepaymentFileDTO> findOne(Long id) {
        log.debug("Request to get RepaymentFile : {}", id);
        return repaymentFileRepository.findById(id)
            .map(repaymentFileMapper::toDto);
    }

    /**
     * Delete the repaymentFile by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RepaymentFile : {}", id);
        repaymentFileRepository.deleteById(id);
    }

    private List<RepaymentTransferDTO> createPayments(RepaymentFileDTO repaymentFile, OfferDTO offer, Double toRepay) {
        TransferConfigWrapper config = TransferConfigWrapper.builder()
            .offerDTO(offer)
            .centralRepaymentWallet(WalletDTO.builder()
                .msisdn(centralMsisdn).pin(centralPin)
                .build())
            .centralFeesWallet(WalletDTO.builder()
                .msisdn(fmFeesMsisdn).pin(fmFeesPin)
                .build())
            .build();
        RepaymentChain repaymentChain = RepaymentChain.builder()
            .repaymentFile(repaymentFile)
            .transferType(toRepay < getEncours(repaymentFile) ? TransferType.PARTIAL : TransferType.COMPLETE)
            .amount(toRepay)
            .config(config)
            .build();
        repaymentChain.handle();

        String repaymentTryId = UUID.randomUUID().toString();
        List<RepaymentTransferDTO> repaymentTransferDTOList = new ArrayList<>();
        for (RepaymentTransferDTO repaymentTransferDTO : repaymentChain.getTransfers()) {
            if(repaymentTransferDTO.getAmount() > 0){
                repaymentTransferDTO.setPaymentMessage(repaymentTryId);
                repaymentTransferDTOList.add(repaymentTransferDTO);
            }
        }
        return repaymentTransferDTOList
            .stream()
            .map(repaymentTransferService::save)
            .collect(Collectors.toList());
    }

    @Override
    public List<RepaymentTransferDTO> payment(Long id, Long amount) {
        final RepaymentFileDTO repaymentFile = this.findOne(id).orElseThrow(IllegalArgumentException::new);
        final ResponseEntity<OfferDTO> offerConfig = this.gatewayClient.getOfferConfig(repaymentFile.getOfferSlug());
        OfferDTO offer;
        if (offerConfig.getStatusCode().is2xxSuccessful()) {
            offer = offerConfig.getBody();
        } else {
            throw new IllegalStateException("Offre pas disponible");
        }
        if (offer == null) throw new IllegalStateException("Offre pas disponible");

        Double encours = getEncours(repaymentFile);
        Double toRepay;
        if (amount == null) {
            toRepay = encours;
        } else {
            if (amount == 0 || amount > encours)
                toRepay = encours;
            else
                toRepay = Double.valueOf(amount);
        }
        return createPayments(repaymentFile, offer, toRepay);
    }

    @Override
    public RepaymentFileDTO paymentOK(Long id, @NotNull Long amount) {
        final RepaymentFileDTO repaymentFile = this.findOne(id).orElseThrow(IllegalArgumentException::new);

        Double encours = getEncours(repaymentFile);
        Double toRepay;
        if (amount == null) {
            toRepay = encours;
        } else {
            if (amount == 0 || amount > encours)
                toRepay = encours;
            else
                toRepay = Double.valueOf(amount);
        }
        final RepaymentFileDTO validatedFile = validatePayment(repaymentFile, toRepay);
        encours = getEncours(repaymentFile);
        if (encours > 0) {
            validatedFile.setRepaymentStatus(RepaymentStatus.PARTIALLY);
        }
        if (encours <= 0) {
            switch (validatedFile.getRepaymentState()) {
                case ENCOURS:
                    validatedFile.setRepaymentStatus(RepaymentStatus.IN_ADVANCE);
                    break;
                case PR1:
                    validatedFile.setRepaymentStatus(RepaymentStatus.PR1);
                    break;
                case PR2:
                    validatedFile.setRepaymentStatus(RepaymentStatus.PR2);
                    break;
                case WRITE_OFF:
                    validatedFile.setRepaymentStatus(RepaymentStatus.OVERDUE);
                    break;
            }
            validatedFile.setRepaymentState(RepaymentState.CLOSED);
        }
        return this.save(validatedFile);
    }

    private RepaymentFileDTO validatePayment(RepaymentFileDTO repaymentFile, Double toRepay) {
        RepaymentChain chain = RepaymentChain.builder()
            .repaymentFile(repaymentFile)
            .transferType(toRepay < getEncours(repaymentFile) ? TransferType.PARTIAL : TransferType.COMPLETE)
            .amount(toRepay)
            .build();
        return chain.validate();
    }

    @Override
    public List<RepaymentFileDTO> findByMsisdnOrRepaymentStateOrRepaymentStatusOrDateBetween(Pageable pageable, String msisdn, RepaymentState state, RepaymentStatus status, Instant debut, Instant fin) {
        List<RepaymentFile> repaymentFileLists = new ArrayList<>();
        if (StringUtils.isBlank(msisdn) && state == null && status == null && debut ==null && fin == null) {
            return this.findAll(Pageable.unpaged()).getContent();
        }
        if (debut != null) {
            if(debut.equals(fin)){
                repaymentFileLists.addAll(repaymentFileRepository.findByCreatedDateAfter(debut));
            }else{
                repaymentFileLists.addAll(repaymentFileRepository.findByCreatedDateBetween(debut, fin));
            }
        }else{
            repaymentFileLists.addAll(repaymentFileRepository.findByCreatedDateBefore(fin));
        }
//        if (StringUtils.isNotBlank(msisdn)) {
//            repaymentFileLists.addAll(repaymentFileRepository.findByMsisdn(msisdn));
//        }
        log.debug("STATE SEARCHED : " + state);
//        if (state != null) {
//            repaymentFileLists.addAll(repaymentFileRepository.findByRepaymentState(state));
//        }
        log.debug("STATUS SEARCHED : " + status);
//        if (status != null) {
//            repaymentFileLists.addAll(repaymentFileRepository.findByRepaymentStatus(status));
//        }
        Stream<RepaymentFileDTO> s = repaymentFileLists.stream()
            .distinct()
            .sorted((l1,l2) -> l2.getCreatedDate().compareTo(l1.getCreatedDate()))
            .map(repaymentFileMapper::toDto);
        if (StringUtils.isNotBlank(msisdn)) {
            s = s.filter(repaymentFileDTO -> repaymentFileDTO.getMsisdn().equals(msisdn));
        }
        if (state != null) {
            s = s.filter(repaymentFileDTO -> repaymentFileDTO.getRepaymentState() == state);
        }
        if (status != null) {
            s = s.filter(repaymentFileDTO -> repaymentFileDTO.getRepaymentStatus() == status);
        }
        return s.collect(Collectors.toList());
    }

    private Double getEncours(RepaymentFileDTO repaymentFile) {

        final Double capitalOutstanding = Optional.ofNullable(repaymentFile.getCapitalOutstanding()).orElse(0.0);
        final Double feesOutstanding = Optional.ofNullable(repaymentFile.getFeesOutstanding()).orElse(0.0);
        final Double pr1Outstanding = Optional.ofNullable(repaymentFile.getPr1Outstanding()).orElse(0.0);
        final Double pr2Outstanding = Optional.ofNullable(repaymentFile.getPr2Outstanding()).orElse(0.0);

        return Math.ceil(capitalOutstanding + feesOutstanding + pr1Outstanding + pr2Outstanding);
    }


    //this automatically update repayment phases and sens sms to subscriber to notify new due date
    @Scheduled(cron = "0 0 8 * * *")
    //@Scheduled(fixedDelay = 60000, initialDelay = 5000)
    public void updateRepaymentFileProgress() {
        log.info("update repayment cron runs");
        this.repaymentFileRepository.findAllByRepaymentStateIsNotIn(new RepaymentState[]{RepaymentState.WRITE_OFF, RepaymentState.CLOSED})
            .forEach(repaymentFile -> {
                log.info("update repayment file {}", repaymentFile.getId());
                if (updateStateAndDueDate(repaymentFile)){
                    updatePR(repaymentFile);
                }
            });
    }

    //change state of loan to pr1 or pr2 or write off or closed according to encours and payment delay
    private boolean updateStateAndDueDate(RepaymentFile repaymentFile) {
        long loanDuration = ChronoUnit.DAYS.between(LocalDateTime.ofInstant(repaymentFile.getCreatedDate(), ZoneOffset.UTC), LocalDateTime.now());
        log.info("updating state of {}", repaymentFile.getRepaymentState());
        long PR0LimitInDays = repaymentFile.getDefaultLoanDuration(); //10
        long PR1LimitInDays = PR0LimitInDays + repaymentFile.getNbJourPr1(); //10 + 10 = 20
        long PR2LimitInDays = PR1LimitInDays + repaymentFile.getNbJourPr2(); // 20 + 15 = 35
        RepaymentState state = repaymentFile.getRepaymentState();
        if (getEncours(repaymentFileMapper.toDto(repaymentFile)) <= 0.0) {
            state = RepaymentState.CLOSED;
        } else {
            if (loanDuration > PR0LimitInDays && loanDuration <= PR1LimitInDays) {
                state = RepaymentState.PR1;
            }
            if (loanDuration > PR1LimitInDays && loanDuration <= PR2LimitInDays) {
                state = RepaymentState.PR2;
            }
            if (loanDuration > PR2LimitInDays) {
                state = RepaymentState.WRITE_OFF;
            }
        }
        //sms jour echeance remboursement apres un remboursement partiel
        //"Il vous reste a rembourser XXX XXX F sur votre Prêt n:XXXXXXXXXX, avant ce soir 23h59.Faites un depot sur votre compte FreeMoney pour ne pas payer de frais de Prolongation."
        if(loanDuration==PR0LimitInDays){
            if(repaymentFile.getRepaymentStatus()==RepaymentStatus.PARTIALLY){
                double enCours = getEncours(repaymentFileMapper.toDto(repaymentFile));
                this.talendClient.sendSms(repaymentFile.getMsisdn(), "Il vous reste a rembourser " + String.valueOf(enCours) + " F sur votre LEBALMA n:" + repaymentFile.getLoanFileID() + ", avant ce soir 23h59. Faites un depot sur votre compte Free Money pour ne pas payer de frais de Prolongation.");
            }
        }
        //this must be done every time the cron is run
        if(loanDuration>PR1LimitInDays && loanDuration<=210){
            //SEND SMS RAPPEL QUOTIDIEN PRET IMPAYE
            this.talendClient.sendSms(repaymentFile.getMsisdn(), "Votre LEBALMA impaye a ete declare au service contentieux. Tapez #150*7# pour regulariser votre situation et redevenir immediatement eligible.");
        }
        boolean changed = repaymentFile.getRepaymentState() != state;
        if (changed) {
            log.info("repayment file {} state has changed from {} to {}", repaymentFile.getId(), repaymentFile.getRepaymentState(), state);
            repaymentFile.setRepaymentState(state);
            LocalDateTime initialDueDate = LocalDateTime.ofInstant(repaymentFile.getDueDate(), ZoneOffset.UTC);
            if(repaymentFile.getRepaymentState()==RepaymentState.PR1){
                Instant dueDate = initialDueDate.plusDays(Long.valueOf(repaymentFile.getNbJourPr1()))
                    .toInstant(ZoneOffset.UTC);
                repaymentFile.setDueDate(dueDate);
            }
            if(repaymentFile.getRepaymentState()==RepaymentState.PR2){
                Instant dueDate = initialDueDate
                    .plusDays(Long.valueOf(repaymentFile.getNbJourPr2()))
                    .toInstant(ZoneOffset.UTC);
                repaymentFile.setDueDate(dueDate);
            }
            if(repaymentFile.getRepaymentState()==RepaymentState.WRITE_OFF){
                boolean blackListUnpayerSucceeded = this.blackListUnpayer(repaymentFile.getMsisdn(), repaymentFile.getIdentificationNo());
                log.info("blacklist unpayer {}, status {}", repaymentFile.getMsisdn(), blackListUnpayerSucceeded);
                if(!blackListUnpayerSucceeded){
                    return false;
                }
                repaymentFile.setRepaymentStatus(RepaymentStatus.OVERDUE);
            }
        }else {
            log.info("repayment file {} state hasnt changed, was {} and still {}", repaymentFile.getId(), repaymentFile.getRepaymentState(), state);
        }
        log.info("new updated value {}", repaymentFile.getRepaymentState());
        this.repaymentFileRepository.save(repaymentFile);
        return changed;
    }

    //update pr1 or pr2 fees and outstanding
    private void updatePR(RepaymentFile repaymentFile) {
        final Double encours = getEncours(repaymentFileMapper.toDto(repaymentFile));
        if (repaymentFile.getRepaymentState() == RepaymentState.PR1) {
            double pr1 = Math.ceil(encours * repaymentFile.getPr1Rate() / 100);
            repaymentFile.setPr1Fees(pr1);
            repaymentFile.setPr1Outstanding(pr1);
            this.talendClient.sendSms(repaymentFile.getMsisdn(),
                "Cher client, vous avez depasse le delai de " + repaymentFile.getDefaultLoanDuration() + " jours pour rembourser votre LEBALMA. Une penalite de " + repaymentFile.getPr1Rate() +"% a ete appliquee. Le nouveau montant a payer est de " + getEncours(repaymentFileMapper.toDto(repaymentFile)) + " FCFA");
        }
        if (repaymentFile.getRepaymentState() == RepaymentState.PR2) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime dueDate = LocalDateTime.ofInstant(repaymentFile.getDueDate(), ZoneOffset.UTC);
            Long numeroPret = repaymentFile.getLoanFileID();
            String date = dueDate.format(formatter);
            double pr2 = Math.ceil(encours * repaymentFile.getPr2Rate() / 100);
            repaymentFile.setPr2Fees(pr2);
            repaymentFile.setPr2Outstanding(pr2);
            //Cher client, vous avez depasse le delai de 30 jours pour rembourser votre prêt depuis le jj mm aaaa. Une penalite de 20% a ete appliquee. Le nouveau montant a payer est de xxxxxFCFA
            this.talendClient.sendSms(repaymentFile.getMsisdn(),"Votre LEBALMA n: " + numeroPret + " a expire depuis le " + date + ". Tapez le #150*7# pour effectuer le remboursement.");
        }
        //SEND SMS BLACKLISTAGE
        //send sms if state changed and loan is in WRITE OFF state
        //This means this will be done only the first time loan doug ci write off state
        //yeineine yoone yi changed will not be true so updatePR will not be run so sms will not be sent
        if(repaymentFile.getRepaymentState()==RepaymentState.WRITE_OFF){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime dueDate = LocalDateTime.ofInstant(repaymentFile.getDueDate(), ZoneOffset.UTC);
            Long numeroPret = repaymentFile.getLoanFileID();
            String date = dueDate.format(formatter);
            //send sms1
            this.talendClient.sendSms(repaymentFile.getMsisdn(),"Votre LEBALMA n: " + numeroPret + " devait etre rembourse le " + date + ". Vous n etes plus autorise a faire un LEBALMA avec votre CNI " + repaymentFile.getIdentificationNo() + ".");
            //send sms2
            this.talendClient.sendSms(repaymentFile.getMsisdn(), "Pour rembourser immediatemment votre LEBALMA n: " + numeroPret + ", faites un depot sur votre compte Free Money et remboursez en composant #150*7#.");
        }
        this.repaymentFileRepository.save(repaymentFile);
    }

    private boolean blackListUnpayer(String msisdn, String cni) {;
        return this.gatewayClient.blackListUnpayer(msisdn, cni, jwt).getStatusCode().is2xxSuccessful();
    }

}
