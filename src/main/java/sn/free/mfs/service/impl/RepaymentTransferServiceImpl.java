package sn.free.mfs.service.impl;

import sn.free.mfs.domain.enumeration.PaymentState;
import sn.free.mfs.service.LoanUtils;
import sn.free.mfs.service.RepaymentTransferService;
import sn.free.mfs.domain.RepaymentTransfer;
import sn.free.mfs.repository.RepaymentTransferRepository;
import sn.free.mfs.service.dto.RepaymentTransferDTO;
import sn.free.mfs.service.mapper.RepaymentTransferMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link RepaymentTransfer}.
 */
@Service
@Transactional
public class RepaymentTransferServiceImpl implements RepaymentTransferService {

    private final Logger log = LoggerFactory.getLogger(RepaymentTransferServiceImpl.class);

    private final RepaymentTransferRepository repaymentTransferRepository;

    private final RepaymentTransferMapper repaymentTransferMapper;

    public RepaymentTransferServiceImpl(RepaymentTransferRepository repaymentTransferRepository, RepaymentTransferMapper repaymentTransferMapper) {
        this.repaymentTransferRepository = repaymentTransferRepository;
        this.repaymentTransferMapper = repaymentTransferMapper;
    }

    /**
     * Save a repaymentTransfer.
     *
     * @param repaymentTransferDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RepaymentTransferDTO save(RepaymentTransferDTO repaymentTransferDTO) {
        log.debug("Request to save RepaymentTransfer : {}", repaymentTransferDTO);
        if(repaymentTransferDTO.getId() != null){
            RepaymentTransferDTO transfer = findOne(repaymentTransferDTO.getId()).orElseThrow(IllegalAccessError::new);
            String transferAttemptId = transfer.getPaymentMessage();
            try{
                LoanUtils.merge(repaymentTransferDTO, transfer);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(repaymentTransferDTO.getPaymentState() == PaymentState.SUCCESS){
                repaymentTransferDTO.setPaymentMessage(transferAttemptId);
            }
            else{
                repaymentTransferDTO.setPaymentMessage(transferAttemptId + "|" + repaymentTransferDTO.getPaymentMessage());
            }
        }
        RepaymentTransfer repaymentTransfer = repaymentTransferMapper.toEntity(repaymentTransferDTO);
        repaymentTransfer = repaymentTransferRepository.save(repaymentTransfer);
        return repaymentTransferMapper.toDto(repaymentTransfer);
    }

    /**
     * Get all the repaymentTransfers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RepaymentTransferDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RepaymentTransfers");
        return repaymentTransferRepository.findAllByOrderByCreatedDateDesc(pageable)
            .map(repaymentTransferMapper::toDto);
    }

    /**
     * Get one repaymentTransfer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RepaymentTransferDTO> findOne(Long id) {
        log.debug("Request to get RepaymentTransfer : {}", id);
        return repaymentTransferRepository.findById(id)
            .map(repaymentTransferMapper::toDto);
    }

    /**
     * Delete the repaymentTransfer by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RepaymentTransfer : {}", id);
        repaymentTransferRepository.deleteById(id);
    }

    @Override
    public Optional<List<RepaymentTransferDTO>> findRepaymentTransfers(Long id) {
        List<RepaymentTransfer> allByRepaymentFileId = this.repaymentTransferRepository.findAllByRepaymentFileIdOrderByCreatedDateDesc(id);
        if (allByRepaymentFileId == null || allByRepaymentFileId.isEmpty()) return Optional.empty();
        return Optional.of(allByRepaymentFileId.stream().map(repaymentTransferMapper::toDto).collect(Collectors.toList()));
    }
}
