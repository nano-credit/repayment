package sn.free.mfs.service.utils;

import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.TransferType;
import sn.free.mfs.service.dto.RepaymentFileDTO;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import java.util.List;
import java.util.Optional;

/**
 * @project: microcredit
 * @author: psow on 21/07/2020
 */
@NoArgsConstructor
class PR2Handler extends PaymentHandler {

    PR2Handler(PaymentHandler next) {
        super(next);
    }


    public void handle(RepaymentFileDTO repaymentFile, Double amount, TransferConfigWrapper config, List<RepaymentTransferDTO> transfers){
        final double outstanding = Math.ceil(Optional.ofNullable(repaymentFile.getPr2Outstanding()).orElse(0.0));
        double remainder = amount;
        if (outstanding > 0) {
            if (outstanding <= remainder) {
                transfers.add(RepaymentTransferDTO.builder()
                    .sourceWallet(config.getCentralRepaymentWallet().getMsisdn())
                    .sourcePin(config.getCentralRepaymentWallet().getPin())
                    .targetWallet(config.getOfferDTO().getFeesWallet().getMsisdn())
                    .amount((double) Math.round(outstanding * 0.4))
                    .type(TransferType.PR2)
                    .repaymentFileId(repaymentFile.getId())
                    .build());
                transfers.add(RepaymentTransferDTO.builder()
                    .sourceWallet(config.getCentralRepaymentWallet().getMsisdn())
                    .sourcePin(config.getCentralRepaymentWallet().getPin())
                    .targetWallet(config.getCentralFeesWallet().getMsisdn())
                    .amount((double) Math.round(outstanding * 0.6))
                    .type(TransferType.PR2)
                    .repaymentFileId(repaymentFile.getId())
                    .build());
                remainder = amount - outstanding;
            }else{
                transfers.add(RepaymentTransferDTO.builder()
                    .sourceWallet(config.getCentralRepaymentWallet().getMsisdn())
                    .sourcePin(config.getCentralRepaymentWallet().getPin())
                    .targetWallet(config.getOfferDTO().getFeesWallet().getMsisdn())
                    .amount((double) Math.round(amount * 0.4))
                    .type(TransferType.PR2)
                    .repaymentFileId(repaymentFile.getId())
                    .build());
                transfers.add(RepaymentTransferDTO.builder()
                    .sourceWallet(config.getCentralRepaymentWallet().getMsisdn())
                    .sourcePin(config.getCentralRepaymentWallet().getPin())
                    .targetWallet(config.getCentralFeesWallet().getMsisdn())
                    .amount((double) Math.round(amount * 0.6))
                    .type(TransferType.PR2)
                    .repaymentFileId(repaymentFile.getId())
                    .build());
                remainder = 0.0;
            }
        }
        super.handle(repaymentFile, remainder, config, transfers);
    }

    @Override
    public RepaymentFileDTO validate(RepaymentFileDTO repaymentFile, Double amount) {
        final double outstanding = Math.ceil(Optional.ofNullable(repaymentFile.getPr2Outstanding()).orElse(0.0));
        double remainder = amount;
        if (outstanding > 0) {
            if (outstanding <= remainder) {
                repaymentFile.setPr2Outstanding(0.0);
                remainder = amount - outstanding;
            }else{
                repaymentFile.setPr2Outstanding(Math.ceil(outstanding - amount));
                remainder = 0.0;
            }
        }
        return super.validate(repaymentFile, remainder);
    }
}
