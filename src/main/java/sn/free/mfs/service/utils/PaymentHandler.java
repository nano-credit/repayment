package sn.free.mfs.service.utils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import sn.free.mfs.service.dto.RepaymentFileDTO;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import java.util.List;

/**
 * @project: microcredit
 * @author: psow on 21/07/2020
 */
@AllArgsConstructor
@NoArgsConstructor
public abstract class PaymentHandler {

    private PaymentHandler next;

    public void handle(RepaymentFileDTO repaymentFile, Double amount, TransferConfigWrapper config, List<RepaymentTransferDTO> transfers){
        if(next != null && amount > 0) next.handle(repaymentFile, amount, config, transfers);
    }

    public RepaymentFileDTO validate(RepaymentFileDTO repaymentFile, Double amount){
        if(next != null && amount > 0) return next.validate(repaymentFile, amount);
        return repaymentFile;
    }
}
