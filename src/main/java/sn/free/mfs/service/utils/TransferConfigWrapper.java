package sn.free.mfs.service.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.client.OfferDTO;
import sn.free.mfs.client.WalletDTO;

/**
 * @project: microcredit
 * @author: psow on 22/07/2020
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferConfigWrapper {
    private OfferDTO offerDTO;
    private WalletDTO centralRepaymentWallet;
    private WalletDTO centralFeesWallet;
}
