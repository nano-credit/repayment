package sn.free.mfs.service.utils;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import sn.free.mfs.domain.RepaymentFile;
import sn.free.mfs.domain.enumeration.TransferType;
import sn.free.mfs.service.dto.RepaymentFileDTO;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @project: microcredit
 * @author: psow on 21/07/2020
 */
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class RepaymentChain {

    @Builder.Default
    private PaymentHandler handler = new FDDHandler(new CRDHandler(new PR1Handler(new PR2Handler())));

    @Builder.Default
    @Getter
    @Setter
    private List<RepaymentTransferDTO> transfers = new ArrayList<>();

    @Getter
    private TransferConfigWrapper config;

    private Double amount;
    private RepaymentFileDTO repaymentFile;
    private TransferType transferType;



    public void handle(){
        transfers.add(RepaymentTransferDTO.builder()
            .sourceWallet(repaymentFile.getMsisdn())
            .targetWallet(config.getCentralRepaymentWallet().getMsisdn())
            .amount(amount)
            .type(transferType)
            .repaymentFileId(repaymentFile.getId())
            .build());
        handler.handle(repaymentFile, amount, config, transfers);
    }


    public RepaymentFileDTO validate() {
        return handler.validate(repaymentFile, amount);
    }
}
