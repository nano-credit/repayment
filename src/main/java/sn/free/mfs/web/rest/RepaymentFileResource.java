package sn.free.mfs.web.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageImpl;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;
import sn.free.mfs.service.RepaymentFileService;
import sn.free.mfs.service.dto.RepaymentTransferDTO;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RepaymentFileDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RepaymentFile}.
 */
@RestController
@RequestMapping("/api")
public class RepaymentFileResource {

    private final Logger log = LoggerFactory.getLogger(RepaymentFileResource.class);

    private static final String ENTITY_NAME = "repaymentRepaymentFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RepaymentFileService repaymentFileService;

    public RepaymentFileResource(RepaymentFileService repaymentFileService) {
        this.repaymentFileService = repaymentFileService;
    }

    /**
     * {@code POST  /repayment-files} : Create a new repaymentFile.
     *
     * @param repaymentFileDTO the repaymentFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new repaymentFileDTO, or with status {@code 400 (Bad Request)} if the repaymentFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/repayment-files")
    public ResponseEntity<RepaymentFileDTO> createRepaymentFile(@Valid @RequestBody RepaymentFileDTO repaymentFileDTO) throws URISyntaxException {
        log.debug("REST request to save RepaymentFile : {}", repaymentFileDTO);
        if (repaymentFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new repaymentFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RepaymentFileDTO result = repaymentFileService.save(repaymentFileDTO);
        return ResponseEntity.created(new URI("/api/repayment-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    //SEEMS LIKE THIS IS NEVER USED
    /**
     * {@code POST  /repayment-files/initialize} : Create a new repaymentFile based on a loanfile
     *
     * @param repaymentFileDTO the repaymentFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new repaymentFileDTO, or with status {@code 400 (Bad Request)} if the repaymentFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/repayment-files/initialize")
    public ResponseEntity<RepaymentFileDTO> initRepaymentFile(@Valid @RequestBody RepaymentFileDTO repaymentFileDTO) throws URISyntaxException {
        log.debug("REST request to initialize RepaymentFile : {}", repaymentFileDTO);
        if (repaymentFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new repaymentFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(repaymentFileDTO.getLoanFileID() == null){
            throw new BadRequestAlertException("A new repaymentFile should have  loan file ID ", ENTITY_NAME, "loanfileidempty");
        }
        RepaymentFileDTO result = repaymentFileService.initiate(repaymentFileDTO);
        return ResponseEntity.created(new URI("/api/repayment-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /repayment-files} : Updates an existing repaymentFile.
     *
     * @param repaymentFileDTO the repaymentFileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated repaymentFileDTO,
     * or with status {@code 400 (Bad Request)} if the repaymentFileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the repaymentFileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/repayment-files")
    public ResponseEntity<RepaymentFileDTO> updateRepaymentFile(@Valid @RequestBody RepaymentFileDTO repaymentFileDTO) throws URISyntaxException {
        log.debug("REST request to update RepaymentFile : {}", repaymentFileDTO);
        if (repaymentFileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RepaymentFileDTO result = repaymentFileService.save(repaymentFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, repaymentFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /repayment-files} : get all the repaymentFiles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of repaymentFiles in body.
     */
    @GetMapping("/repayment-files")
    public ResponseEntity<List<RepaymentFileDTO>> getAllRepaymentFiles(Pageable pageable) {
        log.debug("REST request to get a page of RepaymentFiles");
        Page<RepaymentFileDTO> page = repaymentFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /repayment-files/:id} : get the "id" repaymentFile.
     *
     * @param id the id of the repaymentFileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the repaymentFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/repayment-files/{id}")
    public ResponseEntity<RepaymentFileDTO> getRepaymentFile(@PathVariable Long id) {
        log.debug("REST request to get RepaymentFile : {}", id);
        Optional<RepaymentFileDTO> repaymentFileDTO = repaymentFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(repaymentFileDTO);
    }

    /**
     * {@code DELETE  /repayment-files/:id} : delete the "id" repaymentFile.
     *
     * @param id the id of the repaymentFileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/repayment-files/{id}")
    public ResponseEntity<Void> deleteRepaymentFile(@PathVariable Long id) {
        log.debug("REST request to delete RepaymentFile : {}", id);
        repaymentFileService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    /**
     * {@code GET  /repayment-files/:id/payment} : initialize payment of the repayment with  "id" repaymentFile.
     *
     * @param id the id of the repaymentFileDTO to pay.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the repaymentFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/repayment-files/{id}/payment")
    public ResponseEntity<List<RepaymentTransferDTO>> payment(@PathVariable Long id, @RequestParam(required = false) Long amount) {
        log.debug("REST request to get RepaymentFile : {}", id);
        List<RepaymentTransferDTO> transfers = repaymentFileService.payment(id, amount);
        return ResponseEntity.ok(transfers);
    }

    /**
     * {@code GET  /repayment-files/:id/payment} : initialize payment of the repayment with  "id" repaymentFile.
     *
     * @param id the id of the repaymentFileDTO to pay.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the repaymentFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/repayment-files/{id}/payment/validate")
    public ResponseEntity<RepaymentFileDTO> paymentOK(@PathVariable Long id, @RequestParam Long amount) {
        log.debug("REST request to validate a repayment : {} with amount {}", id, amount);
        RepaymentFileDTO transfer = repaymentFileService.paymentOK(id, amount);
        return ResponseEntity.ok(transfer);
    }

    @GetMapping("/repayment-files/search")
    public ResponseEntity<List<RepaymentFileDTO>> getRepaymentFileByMsisdnOrRepaymentStateOrRepaymentStatus(Pageable pageable, @RequestParam (required = false) String msisdn, @RequestParam (required = false) String state, @RequestParam (required = false) String status,
                                                                                                            @RequestParam (required = false) String debut,
                                                                                                            @RequestParam (required = false) String fin) {
        RepaymentState stateEnum = null;
        RepaymentStatus statusEnum = null;
        if(StringUtils.isNotEmpty(state)){
            try {
                stateEnum = RepaymentState.valueOf(state);
            }catch (IllegalArgumentException | NullPointerException iae) {
                log.debug(iae.getMessage());
            }
        }
        if(StringUtils.isNotEmpty(status)){
            try {
                statusEnum = RepaymentStatus.valueOf(status);
            }catch (IllegalArgumentException | NullPointerException iae) {
                log.debug(iae.getMessage());
            }
        }

        log.debug("REST request to get RepaymentFile List : {}", msisdn + " or " + state+" or "+status);
        log.debug("DEBUT :" + debut);
        log.debug("FIN :" + fin);
        Instant debutt = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant finn = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        List<RepaymentFileDTO> all = repaymentFileService.findByMsisdnOrRepaymentStateOrRepaymentStatusOrDateBetween(pageable, msisdn, stateEnum, statusEnum, debutt, finn);
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), all.size());
        final Page<RepaymentFileDTO> page = new PageImpl<>(all.subList(start, end), pageable, all.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
