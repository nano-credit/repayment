package sn.free.mfs.web.rest;

import sn.free.mfs.service.RepaymentTransferService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.RepaymentTransferDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.RepaymentTransfer}.
 */
@RestController
@RequestMapping("/api")
public class RepaymentTransferResource {

    private final Logger log = LoggerFactory.getLogger(RepaymentTransferResource.class);

    private static final String ENTITY_NAME = "repaymentRepaymentTransfer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RepaymentTransferService repaymentTransferService;

    public RepaymentTransferResource(RepaymentTransferService repaymentTransferService) {
        this.repaymentTransferService = repaymentTransferService;
    }

    /**
     * {@code POST  /repayment-transfers} : Create a new repaymentTransfer.
     *
     * @param repaymentTransferDTO the repaymentTransferDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new repaymentTransferDTO, or with status {@code 400 (Bad Request)} if the repaymentTransfer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/repayment-transfers")
    public ResponseEntity<RepaymentTransferDTO> createRepaymentTransfer(@Valid @RequestBody RepaymentTransferDTO repaymentTransferDTO) throws URISyntaxException {
        log.debug("REST request to save RepaymentTransfer : {}", repaymentTransferDTO);
        if (repaymentTransferDTO.getId() != null) {
            throw new BadRequestAlertException("A new repaymentTransfer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RepaymentTransferDTO result = repaymentTransferService.save(repaymentTransferDTO);
        return ResponseEntity.created(new URI("/api/repayment-transfers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /repayment-transfers} : Updates an existing repaymentTransfer.
     *
     * @param repaymentTransferDTO the repaymentTransferDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated repaymentTransferDTO,
     * or with status {@code 400 (Bad Request)} if the repaymentTransferDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the repaymentTransferDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/repayment-transfers")
    public ResponseEntity<RepaymentTransferDTO> updateRepaymentTransfer(@RequestBody RepaymentTransferDTO repaymentTransferDTO) throws URISyntaxException {
        log.debug("REST request to update RepaymentTransfer : {}", repaymentTransferDTO);
        if (repaymentTransferDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RepaymentTransferDTO result = repaymentTransferService.save(repaymentTransferDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, repaymentTransferDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /repayment-transfers} : get all the repaymentTransfers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of repaymentTransfers in body.
     */
    @GetMapping("/repayment-transfers")
    public ResponseEntity<List<RepaymentTransferDTO>> getAllRepaymentTransfers(Pageable pageable) {
        log.debug("REST request to get a page of RepaymentTransfers");
        Page<RepaymentTransferDTO> page = repaymentTransferService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /repayment-transfers/:id} : get the "id" repaymentTransfer.
     *
     * @param id the id of the repaymentTransferDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the repaymentTransferDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/repayment-transfers/{id}")
    public ResponseEntity<RepaymentTransferDTO> getRepaymentTransfer(@PathVariable Long id) {
        log.debug("REST request to get RepaymentTransfer : {}", id);
        Optional<RepaymentTransferDTO> repaymentTransferDTO = repaymentTransferService.findOne(id);
        return ResponseUtil.wrapOrNotFound(repaymentTransferDTO);
    }

    /**
     * {@code DELETE  /repayment-transfers/:id} : delete the "id" repaymentTransfer.
     *
     * @param id the id of the repaymentTransferDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/repayment-transfers/{id}")
    public ResponseEntity<Void> deleteRepaymentTransfer(@PathVariable Long id) {
        log.debug("REST request to delete RepaymentTransfer : {}", id);
        repaymentTransferService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/repayment-transfers/repaymentfile/{id}")
    public ResponseEntity<List<RepaymentTransferDTO>> getRepaymentTransfers(@PathVariable Long id) {
        log.debug("REST request to get RepaymentTransfer of repaymentfile : {}", id);
        Optional<List<RepaymentTransferDTO>> repaymentTransferDTO = repaymentTransferService.findRepaymentTransfers(id);
        return ResponseUtil.wrapOrNotFound(repaymentTransferDTO);
    }
}
