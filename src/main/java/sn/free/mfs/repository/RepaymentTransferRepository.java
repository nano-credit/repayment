package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.RepaymentTransfer;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the RepaymentTransfer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RepaymentTransferRepository extends JpaRepository<RepaymentTransfer, Long> {
    List<RepaymentTransfer> findAllByRepaymentFileId(Long loanFileId);
    List<RepaymentTransfer> findAllByRepaymentFileIdOrderByCreatedDateDesc(Long loanFileId);
    Page<RepaymentTransfer> findAllByOrderByCreatedDateDesc(Pageable pageable);
}
