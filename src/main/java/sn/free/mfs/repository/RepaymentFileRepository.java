package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.RepaymentFile;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.enumeration.RepaymentState;
import sn.free.mfs.domain.enumeration.RepaymentStatus;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data  repository for the RepaymentFile entity.
 */
@Repository
public interface RepaymentFileRepository extends JpaRepository<RepaymentFile, Long> {

    List<RepaymentFile> findAllByRepaymentStateIsNotIn(RepaymentState[] repaymentStates);

    //List<RepaymentFile> findAllByOfferSlugAndMsisdnAndRepaymentStateNotInAndCreatedDateIsBetween(String offerSlug, String msisdn, RepaymentState[] repaymentStates);

    Page<RepaymentFile> findByMsisdnOrRepaymentStateOrRepaymentStatus(Pageable pageable, Long loanFileId, RepaymentState state, RepaymentStatus status);

    Page<RepaymentFile> findAllByOrderByCreatedByDesc(Pageable pageable);

    List<RepaymentFile> findByMsisdn(String msisdn);
    List<RepaymentFile> findByRepaymentState(RepaymentState state);
    List<RepaymentFile> findByRepaymentStatus(RepaymentStatus status);
    List<RepaymentFile> findByCreatedDateBetween(Instant debut, Instant fin);
    List<RepaymentFile> findByCreatedDateBefore(Instant fin);
    List<RepaymentFile> findByCreatedDateAfter(Instant debut);


}
