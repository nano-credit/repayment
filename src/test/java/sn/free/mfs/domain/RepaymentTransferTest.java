package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RepaymentTransferTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RepaymentTransfer.class);
        RepaymentTransfer repaymentTransfer1 = new RepaymentTransfer();
        repaymentTransfer1.setId(1L);
        RepaymentTransfer repaymentTransfer2 = new RepaymentTransfer();
        repaymentTransfer2.setId(repaymentTransfer1.getId());
        assertThat(repaymentTransfer1).isEqualTo(repaymentTransfer2);
        repaymentTransfer2.setId(2L);
        assertThat(repaymentTransfer1).isNotEqualTo(repaymentTransfer2);
        repaymentTransfer1.setId(null);
        assertThat(repaymentTransfer1).isNotEqualTo(repaymentTransfer2);
    }
}
