package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RepaymentFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RepaymentFile.class);
        RepaymentFile repaymentFile1 = new RepaymentFile();
        repaymentFile1.setId(1L);
        RepaymentFile repaymentFile2 = new RepaymentFile();
        repaymentFile2.setId(repaymentFile1.getId());
        assertThat(repaymentFile1).isEqualTo(repaymentFile2);
        repaymentFile2.setId(2L);
        assertThat(repaymentFile1).isNotEqualTo(repaymentFile2);
        repaymentFile1.setId(null);
        assertThat(repaymentFile1).isNotEqualTo(repaymentFile2);
    }
}
