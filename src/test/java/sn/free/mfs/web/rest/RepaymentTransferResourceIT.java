//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.RepaymentApp;
//import sn.free.mfs.domain.RepaymentTransfer;
//import sn.free.mfs.repository.RepaymentTransferRepository;
//import sn.free.mfs.service.RepaymentTransferService;
//import sn.free.mfs.service.dto.RepaymentTransferDTO;
//import sn.free.mfs.service.mapper.RepaymentTransferMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.free.mfs.domain.enumeration.TransferType;
//import sn.free.mfs.domain.enumeration.PaymentState;
///**
// * Integration tests for the {@link RepaymentTransferResource} REST controller.
// */
//@SpringBootTest(classes = RepaymentApp.class)
//public class RepaymentTransferResourceIT {
//
//    private static final String DEFAULT_SOURCE_WALLET = "AAAAAAAAAA";
//    private static final String UPDATED_SOURCE_WALLET = "BBBBBBBBBB";
//
//    private static final String DEFAULT_TARGET_WALLET = "AAAAAAAAAA";
//    private static final String UPDATED_TARGET_WALLET = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_AMOUNT = 1D;
//    private static final Double UPDATED_AMOUNT = 2D;
//
//    private static final TransferType DEFAULT_TYPE = TransferType.PARTIAL;
//    private static final TransferType UPDATED_TYPE = TransferType.COMPLETE;
//
//    private static final PaymentState DEFAULT_PAYMENT_STATE = PaymentState.PENDING;
//    private static final PaymentState UPDATED_PAYMENT_STATE = PaymentState.SUCCESS;
//
//    private static final String DEFAULT_PAYMENT_TX_ID = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_TX_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PAYMENT_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_MESSAGE = "BBBBBBBBBB";
//
//    @Autowired
//    private RepaymentTransferRepository repaymentTransferRepository;
//
//    @Autowired
//    private RepaymentTransferMapper repaymentTransferMapper;
//
//    @Autowired
//    private RepaymentTransferService repaymentTransferService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restRepaymentTransferMockMvc;
//
//    private RepaymentTransfer repaymentTransfer;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final RepaymentTransferResource repaymentTransferResource = new RepaymentTransferResource(repaymentTransferService);
//        this.restRepaymentTransferMockMvc = MockMvcBuilders.standaloneSetup(repaymentTransferResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RepaymentTransfer createEntity(EntityManager em) {
//        RepaymentTransfer repaymentTransfer = new RepaymentTransfer()
//            .sourceWallet(DEFAULT_SOURCE_WALLET)
//            .targetWallet(DEFAULT_TARGET_WALLET)
//            .amount(DEFAULT_AMOUNT)
//            .type(DEFAULT_TYPE)
//            .paymentState(DEFAULT_PAYMENT_STATE)
//            .paymentTxId(DEFAULT_PAYMENT_TX_ID)
//            .paymentMessage(DEFAULT_PAYMENT_MESSAGE);
//        return repaymentTransfer;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RepaymentTransfer createUpdatedEntity(EntityManager em) {
//        RepaymentTransfer repaymentTransfer = new RepaymentTransfer()
//            .sourceWallet(UPDATED_SOURCE_WALLET)
//            .targetWallet(UPDATED_TARGET_WALLET)
//            .amount(UPDATED_AMOUNT)
//            .type(UPDATED_TYPE)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        return repaymentTransfer;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        repaymentTransfer = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createRepaymentTransfer() throws Exception {
//        int databaseSizeBeforeCreate = repaymentTransferRepository.findAll().size();
//
//        // Create the RepaymentTransfer
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//        restRepaymentTransferMockMvc.perform(post("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the RepaymentTransfer in the database
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeCreate + 1);
//        RepaymentTransfer testRepaymentTransfer = repaymentTransferList.get(repaymentTransferList.size() - 1);
//        assertThat(testRepaymentTransfer.getSourceWallet()).isEqualTo(DEFAULT_SOURCE_WALLET);
//        assertThat(testRepaymentTransfer.getTargetWallet()).isEqualTo(DEFAULT_TARGET_WALLET);
//        assertThat(testRepaymentTransfer.getAmount()).isEqualTo(DEFAULT_AMOUNT);
//        assertThat(testRepaymentTransfer.getType()).isEqualTo(DEFAULT_TYPE);
//        assertThat(testRepaymentTransfer.getPaymentState()).isEqualTo(DEFAULT_PAYMENT_STATE);
//        assertThat(testRepaymentTransfer.getPaymentTxId()).isEqualTo(DEFAULT_PAYMENT_TX_ID);
//        assertThat(testRepaymentTransfer.getPaymentMessage()).isEqualTo(DEFAULT_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void createRepaymentTransferWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = repaymentTransferRepository.findAll().size();
//
//        // Create the RepaymentTransfer with an existing ID
//        repaymentTransfer.setId(1L);
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restRepaymentTransferMockMvc.perform(post("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RepaymentTransfer in the database
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkSourceWalletIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentTransferRepository.findAll().size();
//        // set the field null
//        repaymentTransfer.setSourceWallet(null);
//
//        // Create the RepaymentTransfer, which fails.
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//
//        restRepaymentTransferMockMvc.perform(post("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkTargetWalletIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentTransferRepository.findAll().size();
//        // set the field null
//        repaymentTransfer.setTargetWallet(null);
//
//        // Create the RepaymentTransfer, which fails.
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//
//        restRepaymentTransferMockMvc.perform(post("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkAmountIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentTransferRepository.findAll().size();
//        // set the field null
//        repaymentTransfer.setAmount(null);
//
//        // Create the RepaymentTransfer, which fails.
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//
//        restRepaymentTransferMockMvc.perform(post("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllRepaymentTransfers() throws Exception {
//        // Initialize the database
//        repaymentTransferRepository.saveAndFlush(repaymentTransfer);
//
//        // Get all the repaymentTransferList
//        restRepaymentTransferMockMvc.perform(get("/api/repayment-transfers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(repaymentTransfer.getId().intValue())))
//            .andExpect(jsonPath("$.[*].sourceWallet").value(hasItem(DEFAULT_SOURCE_WALLET)))
//            .andExpect(jsonPath("$.[*].targetWallet").value(hasItem(DEFAULT_TARGET_WALLET)))
//            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
//            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].paymentState").value(hasItem(DEFAULT_PAYMENT_STATE.toString())))
//            .andExpect(jsonPath("$.[*].paymentTxId").value(hasItem(DEFAULT_PAYMENT_TX_ID)))
//            .andExpect(jsonPath("$.[*].paymentMessage").value(hasItem(DEFAULT_PAYMENT_MESSAGE)));
//    }
//
//    @Test
//    @Transactional
//    public void getRepaymentTransfer() throws Exception {
//        // Initialize the database
//        repaymentTransferRepository.saveAndFlush(repaymentTransfer);
//
//        // Get the repaymentTransfer
//        restRepaymentTransferMockMvc.perform(get("/api/repayment-transfers/{id}", repaymentTransfer.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(repaymentTransfer.getId().intValue()))
//            .andExpect(jsonPath("$.sourceWallet").value(DEFAULT_SOURCE_WALLET))
//            .andExpect(jsonPath("$.targetWallet").value(DEFAULT_TARGET_WALLET))
//            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
//            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
//            .andExpect(jsonPath("$.paymentState").value(DEFAULT_PAYMENT_STATE.toString()))
//            .andExpect(jsonPath("$.paymentTxId").value(DEFAULT_PAYMENT_TX_ID))
//            .andExpect(jsonPath("$.paymentMessage").value(DEFAULT_PAYMENT_MESSAGE));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingRepaymentTransfer() throws Exception {
//        // Get the repaymentTransfer
//        restRepaymentTransferMockMvc.perform(get("/api/repayment-transfers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateRepaymentTransfer() throws Exception {
//        // Initialize the database
//        repaymentTransferRepository.saveAndFlush(repaymentTransfer);
//
//        int databaseSizeBeforeUpdate = repaymentTransferRepository.findAll().size();
//
//        // Update the repaymentTransfer
//        RepaymentTransfer updatedRepaymentTransfer = repaymentTransferRepository.findById(repaymentTransfer.getId()).get();
//        // Disconnect from session so that the updates on updatedRepaymentTransfer are not directly saved in db
//        em.detach(updatedRepaymentTransfer);
//        updatedRepaymentTransfer
//            .sourceWallet(UPDATED_SOURCE_WALLET)
//            .targetWallet(UPDATED_TARGET_WALLET)
//            .amount(UPDATED_AMOUNT)
//            .type(UPDATED_TYPE)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(updatedRepaymentTransfer);
//
//        restRepaymentTransferMockMvc.perform(put("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the RepaymentTransfer in the database
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeUpdate);
//        RepaymentTransfer testRepaymentTransfer = repaymentTransferList.get(repaymentTransferList.size() - 1);
//        assertThat(testRepaymentTransfer.getSourceWallet()).isEqualTo(UPDATED_SOURCE_WALLET);
//        assertThat(testRepaymentTransfer.getTargetWallet()).isEqualTo(UPDATED_TARGET_WALLET);
//        assertThat(testRepaymentTransfer.getAmount()).isEqualTo(UPDATED_AMOUNT);
//        assertThat(testRepaymentTransfer.getType()).isEqualTo(UPDATED_TYPE);
//        assertThat(testRepaymentTransfer.getPaymentState()).isEqualTo(UPDATED_PAYMENT_STATE);
//        assertThat(testRepaymentTransfer.getPaymentTxId()).isEqualTo(UPDATED_PAYMENT_TX_ID);
//        assertThat(testRepaymentTransfer.getPaymentMessage()).isEqualTo(UPDATED_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingRepaymentTransfer() throws Exception {
//        int databaseSizeBeforeUpdate = repaymentTransferRepository.findAll().size();
//
//        // Create the RepaymentTransfer
//        RepaymentTransferDTO repaymentTransferDTO = repaymentTransferMapper.toDto(repaymentTransfer);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restRepaymentTransferMockMvc.perform(put("/api/repayment-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentTransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RepaymentTransfer in the database
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteRepaymentTransfer() throws Exception {
//        // Initialize the database
//        repaymentTransferRepository.saveAndFlush(repaymentTransfer);
//
//        int databaseSizeBeforeDelete = repaymentTransferRepository.findAll().size();
//
//        // Delete the repaymentTransfer
//        restRepaymentTransferMockMvc.perform(delete("/api/repayment-transfers/{id}", repaymentTransfer.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<RepaymentTransfer> repaymentTransferList = repaymentTransferRepository.findAll();
//        assertThat(repaymentTransferList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
