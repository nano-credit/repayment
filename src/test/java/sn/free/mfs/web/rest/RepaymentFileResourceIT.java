//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.RepaymentApp;
//import sn.free.mfs.domain.RepaymentFile;
//import sn.free.mfs.repository.RepaymentFileRepository;
//import sn.free.mfs.service.RepaymentFileService;
//import sn.free.mfs.service.dto.RepaymentFileDTO;
//import sn.free.mfs.service.mapper.RepaymentFileMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.time.Instant;
//import java.time.temporal.ChronoUnit;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.free.mfs.domain.enumeration.RepaymentState;
//import sn.free.mfs.domain.enumeration.RepaymentStatus;
///**
// * Integration tests for the {@link RepaymentFileResource} REST controller.
// */
//@SpringBootTest(classes = RepaymentApp.class)
//public class RepaymentFileResourceIT {
//
//    private static final Long DEFAULT_LOAN_FILE_ID = 1L;
//    private static final Long UPDATED_LOAN_FILE_ID = 2L;
//
//    private static final Double DEFAULT_INITIAL_CAPITAL = 1D;
//    private static final Double UPDATED_INITIAL_CAPITAL = 2D;
//
//    private static final Instant DEFAULT_INITIAL_DUE_DATE = Instant.ofEpochMilli(0L);
//    private static final Instant UPDATED_INITIAL_DUE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
//
//    private static final Double DEFAULT_APPLICATION_FEES = 1D;
//    private static final Double UPDATED_APPLICATION_FEES = 2D;
//
//    private static final Double DEFAULT_PR_1_FEES = 1D;
//    private static final Double UPDATED_PR_1_FEES = 2D;
//
//    private static final Double DEFAULT_PR_2_FEES = 1D;
//    private static final Double UPDATED_PR_2_FEES = 2D;
//
//    private static final Double DEFAULT_CAPITAL_OUTSTANDING = 1D;
//    private static final Double UPDATED_CAPITAL_OUTSTANDING = 2D;
//
//    private static final Double DEFAULT_FEES_OUTSTANDING = 1D;
//    private static final Double UPDATED_FEES_OUTSTANDING = 2D;
//
//    private static final Double DEFAULT_PR_1_OUTSTANDING = 1D;
//    private static final Double UPDATED_PR_1_OUTSTANDING = 2D;
//
//    private static final Double DEFAULT_PR_2_OUTSTANDING = 1D;
//    private static final Double UPDATED_PR_2_OUTSTANDING = 2D;
//
//    private static final RepaymentState DEFAULT_REPAYMENT_STATE = RepaymentState.ENCOURS;
//    private static final RepaymentState UPDATED_REPAYMENT_STATE = RepaymentState.PR1;
//
//    private static final RepaymentStatus DEFAULT_REPAYMENT_STATUS = RepaymentStatus.NA;
//    private static final RepaymentStatus UPDATED_REPAYMENT_STATUS = RepaymentStatus.IN_ADVANCE;
//
//    private static final Instant DEFAULT_DUE_DATE = Instant.ofEpochMilli(0L);
//    private static final Instant UPDATED_DUE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
//
//    @Autowired
//    private RepaymentFileRepository repaymentFileRepository;
//
//    @Autowired
//    private RepaymentFileMapper repaymentFileMapper;
//
//    @Autowired
//    private RepaymentFileService repaymentFileService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restRepaymentFileMockMvc;
//
//    private RepaymentFile repaymentFile;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final RepaymentFileResource repaymentFileResource = new RepaymentFileResource(repaymentFileService);
//        this.restRepaymentFileMockMvc = MockMvcBuilders.standaloneSetup(repaymentFileResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RepaymentFile createEntity(EntityManager em) {
//        RepaymentFile repaymentFile = new RepaymentFile()
//            .loanFileID(DEFAULT_LOAN_FILE_ID)
//            .initialCapital(DEFAULT_INITIAL_CAPITAL)
//            .initialDueDate(DEFAULT_INITIAL_DUE_DATE)
//            .applicationFees(DEFAULT_APPLICATION_FEES)
//            .pr1Fees(DEFAULT_PR_1_FEES)
//            .pr2Fees(DEFAULT_PR_2_FEES)
//            .capitalOutstanding(DEFAULT_CAPITAL_OUTSTANDING)
//            .feesOutstanding(DEFAULT_FEES_OUTSTANDING)
//            .pr1Outstanding(DEFAULT_PR_1_OUTSTANDING)
//            .pr2Outstanding(DEFAULT_PR_2_OUTSTANDING)
//            .repaymentState(DEFAULT_REPAYMENT_STATE)
//            .repaymentStatus(DEFAULT_REPAYMENT_STATUS)
//            .dueDate(DEFAULT_DUE_DATE);
//        return repaymentFile;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static RepaymentFile createUpdatedEntity(EntityManager em) {
//        RepaymentFile repaymentFile = new RepaymentFile()
//            .loanFileID(UPDATED_LOAN_FILE_ID)
//            .initialCapital(UPDATED_INITIAL_CAPITAL)
//            .initialDueDate(UPDATED_INITIAL_DUE_DATE)
//            .applicationFees(UPDATED_APPLICATION_FEES)
//            .pr1Fees(UPDATED_PR_1_FEES)
//            .pr2Fees(UPDATED_PR_2_FEES)
//            .capitalOutstanding(UPDATED_CAPITAL_OUTSTANDING)
//            .feesOutstanding(UPDATED_FEES_OUTSTANDING)
//            .pr1Outstanding(UPDATED_PR_1_OUTSTANDING)
//            .pr2Outstanding(UPDATED_PR_2_OUTSTANDING)
//            .repaymentState(UPDATED_REPAYMENT_STATE)
//            .repaymentStatus(UPDATED_REPAYMENT_STATUS)
//            .dueDate(UPDATED_DUE_DATE);
//        return repaymentFile;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        repaymentFile = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createRepaymentFile() throws Exception {
//        int databaseSizeBeforeCreate = repaymentFileRepository.findAll().size();
//
//        // Create the RepaymentFile
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the RepaymentFile in the database
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeCreate + 1);
//        RepaymentFile testRepaymentFile = repaymentFileList.get(repaymentFileList.size() - 1);
//        assertThat(testRepaymentFile.getLoanFileID()).isEqualTo(DEFAULT_LOAN_FILE_ID);
//        assertThat(testRepaymentFile.getInitialCapital()).isEqualTo(DEFAULT_INITIAL_CAPITAL);
//        assertThat(testRepaymentFile.getInitialDueDate()).isEqualTo(DEFAULT_INITIAL_DUE_DATE);
//        assertThat(testRepaymentFile.getApplicationFees()).isEqualTo(DEFAULT_APPLICATION_FEES);
//        assertThat(testRepaymentFile.getPr1Fees()).isEqualTo(DEFAULT_PR_1_FEES);
//        assertThat(testRepaymentFile.getPr2Fees()).isEqualTo(DEFAULT_PR_2_FEES);
//        assertThat(testRepaymentFile.getCapitalOutstanding()).isEqualTo(DEFAULT_CAPITAL_OUTSTANDING);
//        assertThat(testRepaymentFile.getFeesOutstanding()).isEqualTo(DEFAULT_FEES_OUTSTANDING);
//        assertThat(testRepaymentFile.getPr1Outstanding()).isEqualTo(DEFAULT_PR_1_OUTSTANDING);
//        assertThat(testRepaymentFile.getPr2Outstanding()).isEqualTo(DEFAULT_PR_2_OUTSTANDING);
//        assertThat(testRepaymentFile.getRepaymentState()).isEqualTo(DEFAULT_REPAYMENT_STATE);
//        assertThat(testRepaymentFile.getRepaymentStatus()).isEqualTo(DEFAULT_REPAYMENT_STATUS);
//        assertThat(testRepaymentFile.getDueDate()).isEqualTo(DEFAULT_DUE_DATE);
//    }
//
//    @Test
//    @Transactional
//    public void createRepaymentFileWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = repaymentFileRepository.findAll().size();
//
//        // Create the RepaymentFile with an existing ID
//        repaymentFile.setId(1L);
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RepaymentFile in the database
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkLoanFileIDIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setLoanFileID(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkInitialCapitalIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setInitialCapital(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkInitialDueDateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setInitialDueDate(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkApplicationFeesIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setApplicationFees(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkRepaymentStateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setRepaymentState(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkRepaymentStatusIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setRepaymentStatus(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkDueDateIsRequired() throws Exception {
//        int databaseSizeBeforeTest = repaymentFileRepository.findAll().size();
//        // set the field null
//        repaymentFile.setDueDate(null);
//
//        // Create the RepaymentFile, which fails.
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        restRepaymentFileMockMvc.perform(post("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllRepaymentFiles() throws Exception {
//        // Initialize the database
//        repaymentFileRepository.saveAndFlush(repaymentFile);
//
//        // Get all the repaymentFileList
//        restRepaymentFileMockMvc.perform(get("/api/repayment-files?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(repaymentFile.getId().intValue())))
//            .andExpect(jsonPath("$.[*].loanFileID").value(hasItem(DEFAULT_LOAN_FILE_ID.intValue())))
//            .andExpect(jsonPath("$.[*].initialCapital").value(hasItem(DEFAULT_INITIAL_CAPITAL.doubleValue())))
//            .andExpect(jsonPath("$.[*].initialDueDate").value(hasItem(DEFAULT_INITIAL_DUE_DATE.toString())))
//            .andExpect(jsonPath("$.[*].applicationFees").value(hasItem(DEFAULT_APPLICATION_FEES.doubleValue())))
//            .andExpect(jsonPath("$.[*].pr1Fees").value(hasItem(DEFAULT_PR_1_FEES.doubleValue())))
//            .andExpect(jsonPath("$.[*].pr2Fees").value(hasItem(DEFAULT_PR_2_FEES.doubleValue())))
//            .andExpect(jsonPath("$.[*].capitalOutstanding").value(hasItem(DEFAULT_CAPITAL_OUTSTANDING.doubleValue())))
//            .andExpect(jsonPath("$.[*].feesOutstanding").value(hasItem(DEFAULT_FEES_OUTSTANDING.doubleValue())))
//            .andExpect(jsonPath("$.[*].pr1Outstanding").value(hasItem(DEFAULT_PR_1_OUTSTANDING.doubleValue())))
//            .andExpect(jsonPath("$.[*].pr2Outstanding").value(hasItem(DEFAULT_PR_2_OUTSTANDING.doubleValue())))
//            .andExpect(jsonPath("$.[*].repaymentState").value(hasItem(DEFAULT_REPAYMENT_STATE.toString())))
//            .andExpect(jsonPath("$.[*].repaymentStatus").value(hasItem(DEFAULT_REPAYMENT_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].dueDate").value(hasItem(DEFAULT_DUE_DATE.toString())));
//    }
//
//    @Test
//    @Transactional
//    public void getRepaymentFile() throws Exception {
//        // Initialize the database
//        repaymentFileRepository.saveAndFlush(repaymentFile);
//
//        // Get the repaymentFile
//        restRepaymentFileMockMvc.perform(get("/api/repayment-files/{id}", repaymentFile.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(repaymentFile.getId().intValue()))
//            .andExpect(jsonPath("$.loanFileID").value(DEFAULT_LOAN_FILE_ID.intValue()))
//            .andExpect(jsonPath("$.initialCapital").value(DEFAULT_INITIAL_CAPITAL.doubleValue()))
//            .andExpect(jsonPath("$.initialDueDate").value(DEFAULT_INITIAL_DUE_DATE.toString()))
//            .andExpect(jsonPath("$.applicationFees").value(DEFAULT_APPLICATION_FEES.doubleValue()))
//            .andExpect(jsonPath("$.pr1Fees").value(DEFAULT_PR_1_FEES.doubleValue()))
//            .andExpect(jsonPath("$.pr2Fees").value(DEFAULT_PR_2_FEES.doubleValue()))
//            .andExpect(jsonPath("$.capitalOutstanding").value(DEFAULT_CAPITAL_OUTSTANDING.doubleValue()))
//            .andExpect(jsonPath("$.feesOutstanding").value(DEFAULT_FEES_OUTSTANDING.doubleValue()))
//            .andExpect(jsonPath("$.pr1Outstanding").value(DEFAULT_PR_1_OUTSTANDING.doubleValue()))
//            .andExpect(jsonPath("$.pr2Outstanding").value(DEFAULT_PR_2_OUTSTANDING.doubleValue()))
//            .andExpect(jsonPath("$.repaymentState").value(DEFAULT_REPAYMENT_STATE.toString()))
//            .andExpect(jsonPath("$.repaymentStatus").value(DEFAULT_REPAYMENT_STATUS.toString()))
//            .andExpect(jsonPath("$.dueDate").value(DEFAULT_DUE_DATE.toString()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingRepaymentFile() throws Exception {
//        // Get the repaymentFile
//        restRepaymentFileMockMvc.perform(get("/api/repayment-files/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateRepaymentFile() throws Exception {
//        // Initialize the database
//        repaymentFileRepository.saveAndFlush(repaymentFile);
//
//        int databaseSizeBeforeUpdate = repaymentFileRepository.findAll().size();
//
//        // Update the repaymentFile
//        RepaymentFile updatedRepaymentFile = repaymentFileRepository.findById(repaymentFile.getId()).get();
//        // Disconnect from session so that the updates on updatedRepaymentFile are not directly saved in db
//        em.detach(updatedRepaymentFile);
//        updatedRepaymentFile
//            .loanFileID(UPDATED_LOAN_FILE_ID)
//            .initialCapital(UPDATED_INITIAL_CAPITAL)
//            .initialDueDate(UPDATED_INITIAL_DUE_DATE)
//            .applicationFees(UPDATED_APPLICATION_FEES)
//            .pr1Fees(UPDATED_PR_1_FEES)
//            .pr2Fees(UPDATED_PR_2_FEES)
//            .capitalOutstanding(UPDATED_CAPITAL_OUTSTANDING)
//            .feesOutstanding(UPDATED_FEES_OUTSTANDING)
//            .pr1Outstanding(UPDATED_PR_1_OUTSTANDING)
//            .pr2Outstanding(UPDATED_PR_2_OUTSTANDING)
//            .repaymentState(UPDATED_REPAYMENT_STATE)
//            .repaymentStatus(UPDATED_REPAYMENT_STATUS)
//            .dueDate(UPDATED_DUE_DATE);
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(updatedRepaymentFile);
//
//        restRepaymentFileMockMvc.perform(put("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the RepaymentFile in the database
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeUpdate);
//        RepaymentFile testRepaymentFile = repaymentFileList.get(repaymentFileList.size() - 1);
//        assertThat(testRepaymentFile.getLoanFileID()).isEqualTo(UPDATED_LOAN_FILE_ID);
//        assertThat(testRepaymentFile.getInitialCapital()).isEqualTo(UPDATED_INITIAL_CAPITAL);
//        assertThat(testRepaymentFile.getInitialDueDate()).isEqualTo(UPDATED_INITIAL_DUE_DATE);
//        assertThat(testRepaymentFile.getApplicationFees()).isEqualTo(UPDATED_APPLICATION_FEES);
//        assertThat(testRepaymentFile.getPr1Fees()).isEqualTo(UPDATED_PR_1_FEES);
//        assertThat(testRepaymentFile.getPr2Fees()).isEqualTo(UPDATED_PR_2_FEES);
//        assertThat(testRepaymentFile.getCapitalOutstanding()).isEqualTo(UPDATED_CAPITAL_OUTSTANDING);
//        assertThat(testRepaymentFile.getFeesOutstanding()).isEqualTo(UPDATED_FEES_OUTSTANDING);
//        assertThat(testRepaymentFile.getPr1Outstanding()).isEqualTo(UPDATED_PR_1_OUTSTANDING);
//        assertThat(testRepaymentFile.getPr2Outstanding()).isEqualTo(UPDATED_PR_2_OUTSTANDING);
//        assertThat(testRepaymentFile.getRepaymentState()).isEqualTo(UPDATED_REPAYMENT_STATE);
//        assertThat(testRepaymentFile.getRepaymentStatus()).isEqualTo(UPDATED_REPAYMENT_STATUS);
//        assertThat(testRepaymentFile.getDueDate()).isEqualTo(UPDATED_DUE_DATE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingRepaymentFile() throws Exception {
//        int databaseSizeBeforeUpdate = repaymentFileRepository.findAll().size();
//
//        // Create the RepaymentFile
//        RepaymentFileDTO repaymentFileDTO = repaymentFileMapper.toDto(repaymentFile);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restRepaymentFileMockMvc.perform(put("/api/repayment-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(repaymentFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the RepaymentFile in the database
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteRepaymentFile() throws Exception {
//        // Initialize the database
//        repaymentFileRepository.saveAndFlush(repaymentFile);
//
//        int databaseSizeBeforeDelete = repaymentFileRepository.findAll().size();
//
//        // Delete the repaymentFile
//        restRepaymentFileMockMvc.perform(delete("/api/repayment-files/{id}", repaymentFile.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<RepaymentFile> repaymentFileList = repaymentFileRepository.findAll();
//        assertThat(repaymentFileList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
