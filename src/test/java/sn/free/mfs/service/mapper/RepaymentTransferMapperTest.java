package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RepaymentTransferMapperTest {

    private RepaymentTransferMapper repaymentTransferMapper;

    @BeforeEach
    public void setUp() {
        repaymentTransferMapper = new RepaymentTransferMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(repaymentTransferMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(repaymentTransferMapper.fromId(null)).isNull();
    }
}
