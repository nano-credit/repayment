package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RepaymentFileMapperTest {

    private RepaymentFileMapper repaymentFileMapper;

    @BeforeEach
    public void setUp() {
        repaymentFileMapper = new RepaymentFileMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(repaymentFileMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(repaymentFileMapper.fromId(null)).isNull();
    }
}
