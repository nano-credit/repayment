package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RepaymentTransferDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RepaymentTransferDTO.class);
        RepaymentTransferDTO repaymentTransferDTO1 = new RepaymentTransferDTO();
        repaymentTransferDTO1.setId(1L);
        RepaymentTransferDTO repaymentTransferDTO2 = new RepaymentTransferDTO();
        assertThat(repaymentTransferDTO1).isNotEqualTo(repaymentTransferDTO2);
        repaymentTransferDTO2.setId(repaymentTransferDTO1.getId());
        assertThat(repaymentTransferDTO1).isEqualTo(repaymentTransferDTO2);
        repaymentTransferDTO2.setId(2L);
        assertThat(repaymentTransferDTO1).isNotEqualTo(repaymentTransferDTO2);
        repaymentTransferDTO1.setId(null);
        assertThat(repaymentTransferDTO1).isNotEqualTo(repaymentTransferDTO2);
    }
}
