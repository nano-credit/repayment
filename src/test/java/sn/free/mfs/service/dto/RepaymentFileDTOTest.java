package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class RepaymentFileDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RepaymentFileDTO.class);
        RepaymentFileDTO repaymentFileDTO1 = new RepaymentFileDTO();
        repaymentFileDTO1.setId(1L);
        RepaymentFileDTO repaymentFileDTO2 = new RepaymentFileDTO();
        assertThat(repaymentFileDTO1).isNotEqualTo(repaymentFileDTO2);
        repaymentFileDTO2.setId(repaymentFileDTO1.getId());
        assertThat(repaymentFileDTO1).isEqualTo(repaymentFileDTO2);
        repaymentFileDTO2.setId(2L);
        assertThat(repaymentFileDTO1).isNotEqualTo(repaymentFileDTO2);
        repaymentFileDTO1.setId(null);
        assertThat(repaymentFileDTO1).isNotEqualTo(repaymentFileDTO2);
    }
}
